package facci.carlosgomez.tvseries;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText nombre, categoria, protagonistas, añodetransmicion, numerodetemporada, id;
    private Button guardar, leer, leerT, actualizar, Eliminar, eliminarT;
    private TextView Datos;
    private ClassHelper dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nombre = (EditText)findViewById(R.id.nombre);
        categoria = (EditText)findViewById(R.id.categoria);
        protagonistas = (EditText)findViewById(R.id.protagonistas);
        añodetransmicion = (EditText)findViewById(R.id.añotransmision);
        numerodetemporada = (EditText)findViewById(R.id.numerodetemporada);
        id = (EditText)findViewById(R.id.Id);
        guardar = (Button)findViewById(R.id.Guardar);
        leerT = (Button)findViewById(R.id.LeerT);
        actualizar = (Button)findViewById(R.id.Actualizar);
        eliminarT = (Button)findViewById(R.id.EliminarT);
        leer = (Button)findViewById(R.id.Leer);

        guardar.setOnClickListener(this);
        leerT.setOnClickListener(this);
        leer.setOnClickListener(this);
        actualizar.setOnClickListener(this);
        eliminarT.setOnClickListener(this);
        Datos = (TextView)findViewById(R.id.Datos);
        dataBase = new ClassHelper(this);



    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.Guardar:

                if (nombre.getText().toString().isEmpty()){

                }else if(nombre.getText().toString().isEmpty()){

                }else if(categoria.getText().toString().isEmpty()){

                }else if (protagonistas.getText().toString().isEmpty()){

                }else if(añodetransmicion.getText().toString().isEmpty()){

                }else if(numerodetemporada.getText().toString().isEmpty()){

                }else {
                    dataBase.Insertar(nombre.getText().toString(), categoria.getText().toString(),
                            protagonistas.getText().toString(), añodetransmicion.getText().toString(), numerodetemporada.getText().toString());
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    nombre.setText("");
                    categoria.setText("");
                    protagonistas.setText("");
                    añodetransmicion.setText("");
                    numerodetemporada.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.LeerT:
                Datos.setText(dataBase.LeerTodos());
                id.setText("");
                nombre.setText("");
                categoria.setText("");
                protagonistas.setText("");
                añodetransmicion.setText("");
                numerodetemporada.setText("");
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show();
                break;

            case R.id.Leer:
                Datos.setText(dataBase.Leer(id.getText().toString()));
                id.setText("");
                nombre.setText("");
                categoria.setText("");
                protagonistas.setText("");
                añodetransmicion.setText("");
                numerodetemporada.setText("");
                Toast.makeText(this, "LISTADO", Toast.LENGTH_SHORT).show();

                break;

            case R.id.Actualizar:
                if (nombre.getText().toString().isEmpty()){

                }else if(nombre.getText().toString().isEmpty()){

                }else if(categoria.getText().toString().isEmpty()){

                }else if (protagonistas.getText().toString().isEmpty()){

                }else if(añodetransmicion.getText().toString().isEmpty()){

                }else if(numerodetemporada.getText().toString().isEmpty()){

                }else if(id.getText().toString().isEmpty()){

                }else {
                    dataBase.Actualizar(id.getText().toString(), nombre.getText().toString(), categoria.getText().toString(),
                            protagonistas.getText().toString(), añodetransmicion.getText().toString(), numerodetemporada.getText().toString());
                    Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    nombre.setText("");
                    categoria.setText("");
                    protagonistas.setText("");
                    añodetransmicion.setText("");
                    numerodetemporada.setText("");
                    Datos.setText("");
                }
                break;

            case R.id.EliminarT:
                dataBase.EliminarTodo();
                Datos.setText("");
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show();
                break;
        }
    }
    }