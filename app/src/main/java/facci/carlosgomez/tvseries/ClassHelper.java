package facci.carlosgomez.tvseries;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class ClassHelper extends SQLiteOpenHelper {

    private static int version = 1;
    private static String name = "Prueba" ;
    private static CursorFactory factory = null;

    public ClassHelper(Context context) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE Series(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Nombre VARCHAR," +
                "Categoria VARCHAR," +
                "Protagonistas VARCHAR," +
                "AñoDeTransmision VARCHAR, " +
                "NumeroDeTemporada VARCHAR)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Series";
        db.execSQL(sql);
        onCreate(db);
    }
    public void Insertar(String Nombre, String Categoria, String Protagonistas, String AñoDeTransmision, String NumeroDeTemporada){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("Nombre", Nombre);
        contentValues.put("Categoria", Categoria);
        contentValues.put("Protagonistas", Protagonistas);
        contentValues.put("AñoDeTransmision", AñoDeTransmision);
        contentValues.put("NumeroDeTemporada", NumeroDeTemporada);
        database.insert("Series",null,contentValues);
    }

    public void EliminarTodo(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("Series", null, null);
    }

    public void Actualizar(String id, String Nombre, String Categoria, String Protagonistas, String AñoDeTransmision, String NumeroDeTemporada){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Nombre", Nombre);
        contentValues.put("Categoria", Categoria);
        contentValues.put("Protagonistas", Protagonistas);
        contentValues.put("AñoDeTransmision", AñoDeTransmision);
        contentValues.put("NumeroDeTemporada", NumeroDeTemporada);
        database.update("Series", contentValues, "id= " + id, null);
    }

   public String Leer(String id){
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {"id", "Nombre", "Categoria", "Protagonistas", "AñoDeTransmision", "NumeroDeTemporada"};
        String sqlTable = "Series";
        String[] args = new String[] {id};
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, "id=?", args, null, null, null);
        if (c.moveToFirst()) {
            do {
                result +="ID " + c.getInt(c.getColumnIndex("id")) + "\n" +
                        "Nombre " + c.getString(c.getColumnIndex("Nombre")) + "\n" +
                        "Categoria " + c.getString(c.getColumnIndex("CategoriaP")) + "\n" +
                        "Protagonistas " + c.getString(c.getColumnIndex("Protagonistas")) + "\n" +
                        "AñoDeTransmision " + c.getString(c.getColumnIndex("AñoDeTransmision")) + "\n" +
                        "NumeroDeTemporada " + c.getString(c.getColumnIndex("NumeroDeTemporada")) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }
    public String LeerTodos() {
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {"id", "Nombre", "Categoria", "Protagonistas", "AñoDeTransmision", "NumeroDeTemporada"};
        String sqlTable = "Series";
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                result +="id " + c.getInt(c.getColumnIndex("id")) + "\n" +
                        "Nombre " + c.getString(c.getColumnIndex("Nombre")) + "\n" +
                        "Categoria " + c.getString(c.getColumnIndex("CategoriaP")) + "\n" +
                        "Protagonistas " + c.getString(c.getColumnIndex("Protagonistas")) + "\n" +
                        "AñoDeTransmision " + c.getString(c.getColumnIndex("AñoDeTransmision")) + "\n" +
                        "NumeroDeTemporada " + c.getString(c.getColumnIndex("NumeroDeTemporada")) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }
}